#version 330 core
layout (location = 0) in vec2 position;
layout (location = 2) in vec2 aTexCoord;

out vec2 TexCoord;

void main()
{
	TexCoord = aTexCoord;
	gl_Position = vec4(position.x,  -position.y, 1.0, 1.0);
}
