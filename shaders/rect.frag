#version 330 core

in vec2 TexCoord;
uniform sampler2D tex1;

out vec4 color;
void main()
{
	color = texture(tex1, TexCoord) * vec4(1.0, 1.0, 1.0, 1.0);
}
