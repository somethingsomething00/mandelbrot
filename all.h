#ifndef _ALL_H_
#define _ALL_H_

// Standard
//--------------------------------------------------------------------------------
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <pthread.h>
#include <unistd.h>
#include <assert.h>
#include <stdarg.h>



// GL
//--------------------------------------------------------------------------------
#include <GL/glew.h>
#include <GL/gl.h>
#include <GLFW/glfw3.h>

// X86
//--------------------------------------------------------------------------------
#include <immintrin.h>


// Local
//--------------------------------------------------------------------------------
#include "./types.h"
#include "./basictimer.h"
#include "./fpscounter.h"
#include "./loader.h"

#endif /* _ALL_H_ */
