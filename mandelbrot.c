// NAME: mandelbrot.c
// ORIGINAL TIME OF CREATION: 2021-09-10 12:42:52
// AUTHOR:
// DESCRIPTION:
#include "./all.h"
#include "./all.c"


// The psuedocode that started it all...

// For each pixel (Px, Py) on the screen do
//   x0 := scaled x coordinate of pixel (scaled to lie in the Mandelbrot X scale (-2.5, 1))
//   y0 := scaled y coordinate of pixel (scaled to lie in the Mandelbrot Y scale (-1, 1))
//   x := 0.0
//   y := 0.0
//   iteration := 0
//   max_iteration := 1000
//   while (x*x + y*y ≤ 2*2 AND iteration < max_iteration) do
//       xtemp := x*x - y*y + x0
//       y := 2*x*y + y0
//       x := xtemp
//       iteration := iteration + 1
//
//   color := palette[iteration]
//   plot(Px, Py, color)

#define BPP 4

#define KEYPRESS(k) if(key == GLFW_##k && action == GLFW_PRESS)
#define KEYIMM(k) if(glfwGetKey(Window, GLFW_##k) == GLFW_PRESS)

#define INLINE_ALWAYS __always_inline
#define INLINE_NEVER __attribute__((noinline))

#define Dowhile(body) do { body } while(0)

#define Assert(cond_) Dowhile(\
if(!(cond_)) {fprintf(stderr, "%s:%d: ", __FILE__, __LINE__); fprintf(stderr, "Assertion failed for expression '%s'\n", #cond_); exit(1);})

#define Unreachable(...) Dowhile(\
	fprintf(stderr, "%s:%d: ", __FILE__, __LINE__); fprintf(stderr, __VA_ARGS__); exit(1);)

#define PlatformAlloc(n, s) calloc((n), (s))
#define PlatformDealloc(p) free(p)

/**********************************
* Enums
**********************************/
#define IsFullscreen() (Options[OPT_FULLSCREEN] == 1)
typedef enum
{
	OPT_FULLSCREEN,
	OPT_COUNT,
} option_t;

typedef enum
{
	GL_TEXTUREMODE_LINEAR,
	GL_TEXTUREMODE_NEAREST,
	GL_TEXTUREMODE_TOTAL,
} gl_texturemode_t;

enum
{
	VSYNC_DISABLED,
	VSYNC_ENABLED,
};


/**********************************
* Function pointers
**********************************/
// Used for the worker functions in the threadpool
// They are the functions that do the work to calculate the actual set
typedef void (FpWorkerFunc)(void *arg);

/**********************************
* Structs
 **********************************/
typedef struct
{
	// Dynamic resolution of window
	int w;
	int h;

	// Texture resolution
	int resx;
	int resy;

	int windowedX;
	int windowedY;

	int windowedWidth;
	int windowedHeight;
} screen_t;

typedef struct
{
	int width;
	int height;
} resolution_t;

typedef struct
{
	GLuint shader;
	GLuint uColor;
	GLuint uSampler;
	GLuint uOffs;
	GLuint vbo;
	GLuint ebo;
	GLuint vao;
} rect_t;

typedef struct
{
	u8 *texture;
	GLubyte *pixels;
	u32 w;
	u32 h;
	u32 bpp;
	u32 size;
	GLuint id;
	GLuint pbo;
	gl_texturemode_t TextureMode;
} texture_t;

typedef struct
{
	u32 yBegin;
	u32 yEnd;
	u32 xBegin;
	u32 xEnd;
	u32 *fractal;
	u32 internalId;
} worker_t;

typedef struct
{
	worker_t *Workers;
	FpWorkerFunc *WorkerFunc;
	pthread_t *threadIds;
	u32 threadCount;
} threadpool_t;

typedef struct
{
	double x;
	double y;
} v2d;

typedef struct
{
	v2d raw;
	v2d world;
	int held;
	double percX; /* Distance of the mouse measured as a percentage from the middle of the screen */
	double percY;
} mouse_t;


/**********************************
* Globals
**********************************/
#define PROG_NAME "Mandelbrot"
#define ITERATIONS_DEFAULT 32
#define ITERATIONS_STEP 32
#define ITERATION_MASK_DECREASE 0x1
#define ITERATION_MASK_INCREASE 0x2


u32 ITERATIONS = ITERATIONS_DEFAULT; /* How many iterations do we start with? */
// How many elements per logical vertex
#define VERT_STRIDE 7
// For the main quad
float verticies [] =
{
//  vert        colour          texture coordinate
	-1, -1, 	1, 1, 1,		0, 0, // 1, 1, (Flipped)
	-1,  1,		1, 1, 1,		0, 1, // 1, 0,
	1,  1,		1, 1, 1,		1, 1, // 0, 0,
	1, -1,		1, 1, 1,		1, 0, // 0, 1,
};

int indicies [] =
{
	0, 1, 2,
	0, 3, 2,
};


// Hold prescaled coordinates for the resolution that we are rendering to
// Maps the mandelbrot space to screen space
double *xs;
double *ys;

// Colour pallete
u32 *pallete;

GLFWwindow *Win;
GLFWmonitor *Mon;
const GLFWvidmode *Vidmode;

// Camera variables
double scale = 1.0;
double scaleInverse = 1.0;
double offX = 0.0;
double offY = 0.0;
static int cameraWasUpdated = 1;

int running;
double elapsedTimeMs;

rect_t MainRect;
texture_t MainTexture;

pthread_barrier_t barrierMain;
pthread_barrier_t barrierWorker;
threadpool_t Threadpool;
int threadtimetoquit;

u32 iterationsNeedUpdate = 0;
u32 texturemodeNeedsUpdate = 0;

mouse_t Mouse = {0};
screen_t Screen = {0};

#define RES_INDEX_DEFAULT 3 // Make the visuals at least somewhat nice as the default!
// #define VIRTUAL_WIDTH 320
// #define VIRTUAL_HEIGHT 200
#define RESOLUTIONS_TOTAL (sizeof Resolutions / sizeof(resolution_t))
#define RES_CYCLE_UP 0x1
#define RES_CYCLE_DOWN 0x2
resolution_t Resolutions[] =
{
	{160, 100},
	{320, 240},
	{640, 480},
	{800, 600},
	{1024, 768},
	{1680, 1050},
	{1920, 1080},
	{3840, 2160},
	{80, 40}, /* Lowest one is last, just for fun */
};
static u32 resIndex;
static int resNeedsUpdate = 0;


option_t Options[OPT_COUNT];
gl_texturemode_t ActiveTexturemode;

GLuint TexturemodeLookup[GL_TEXTUREMODE_TOTAL] = 
{
	[GL_TEXTUREMODE_LINEAR] = GL_LINEAR,
	[GL_TEXTUREMODE_NEAREST] = GL_NEAREST,
};


/**********************************
* Function declarations
**********************************/
// Allocation
void *FcallocImpl(size_t nmemb, size_t size, const char *file, int line);

// Precalculate
void PrescaledCoordinatesAllocAndGen(u32 virtual_width, u32 virtual_height);
void PrescaledCoordinatesFree();
void PalleteAllocAndGen(u32 colorCount);


// Callbacks
void CbResize(GLFWwindow *Window, int x, int y);
void CbKeys(GLFWwindow *Window, int key, int scancode, int action, int mod);
void PollKeysImmediate(GLFWwindow *Window, double dt);
void CbMouseMove(GLFWwindow *Window, double x, double y);
void CbMousePress(GLFWwindow *Window, int button, int action, int mod);
void CbWindowPosition(GLFWwindow *Window, int xpos, int ypos);
void CbWindowClose(GLFWwindow *Window);


// Update
void UpdateIterationsIfNecessary();
void UpdateResolutionIfNecessary();


// Texture
void TextureFreeBuffers(texture_t *Texture);
texture_t Texture2DCreate(u32 w, u32 h, u32 bpp);
void Texture2DReload(texture_t *Texture);
void Texture2DResize(texture_t *Texture, u32 w, u32 h, u32 bpp);


// Rect
rect_t RectCreate(char *sourceVert, char *sourceFrag);
INLINE_ALWAYS void UpdateTexturemodeIfNecessary(texture_t *Texture);
const char *HumanReadableTexturemode(gl_texturemode_t Mode);


// Threadpool
void ThreadpoolInit(threadpool_t *Pool);
void ThreadpoolAssignWorkerAttributes(threadpool_t *Pool, texture_t *Texture);
void ThreadpoolCreateThreads(threadpool_t *Pool, void (*InitialWorkfunc)(void *arg));
void ThreadpoolWorkfuncAssign(threadpool_t *Pool, void (*Workfunc)(void *arg));
void *ThreadpoolFuncHandle(void *arg);
void ThreadpoolDeinit(threadpool_t *Pool);


// Function pointer versions of the worker functions
// They are called within the ThreadpoolFuncHandle function
// The locks are handled before and after they are called, by ThreadpoolFuncHandle
void PoolMandelIntrinFp(void *arg);
void PoolMandelDefaultFp(void *arg);

void PoolShipIntrinFp(void *arg);
void PoolShipDefaultFp(void *arg);


// Mouse
void MouseToWorld(mouse_t *M);
void PollMouse(mouse_t *M, double dt);


// Program
void ProgPrint(const char *fmt, ...);
void Cleanup();



int main()
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

	Mon = glfwGetPrimaryMonitor();
	Vidmode = glfwGetVideoMode(Mon);
	Win = glfwCreateWindow(Vidmode->width, Vidmode->height, "Mandelbrot", Mon, NULL);
	glfwMakeContextCurrent(Win);
	glewInit();
	Options[OPT_FULLSCREEN] = 1;

	// Callbacks
	glfwSetFramebufferSizeCallback(Win, CbResize);
	glfwSetKeyCallback(Win, CbKeys);
	glfwSetCursorPosCallback(Win, CbMouseMove);
	glfwSetMouseButtonCallback(Win, CbMousePress);
	glfwSetWindowPosCallback(Win, CbWindowPosition);
	glfwSetWindowCloseCallback(Win, CbWindowClose);

	// Texture resolution
	resIndex = RES_INDEX_DEFAULT;
	Screen.resx = Resolutions[resIndex].width;
	Screen.resy = Resolutions[resIndex].height;

	// Screen resolution
	Screen.windowedWidth = Vidmode->width / 4;
	Screen.windowedHeight = Vidmode->height / 4;

	PrescaledCoordinatesAllocAndGen(Screen.resx, Screen.resy);
	PalleteAllocAndGen(ITERATIONS);

	// Texture that we render to
	MainRect = RectCreate("shaders/rect.vert", "shaders/rect.frag");
	MainTexture = Texture2DCreate(Screen.resx, Screen.resy, BPP);
	ActiveTexturemode = GL_TEXTUREMODE_NEAREST;

	glClearColor(0,0,0,1);

	ThreadpoolInit(&Threadpool);
	ThreadpoolAssignWorkerAttributes(&Threadpool, &MainTexture);
	ThreadpoolCreateThreads(&Threadpool, PoolMandelIntrinFp);

	glfwSwapInterval(VSYNC_ENABLED);

	printf("Created %dx%d window\nTexture resolution: %dx%d, %d BPP\nTexture mode: %s\n", 
			Vidmode->width, Vidmode->height,
			Screen.resx, Screen.resy,
			BPP,
			HumanReadableTexturemode(ActiveTexturemode));

	printf("Iterations: %d\n", ITERATIONS);
	printf("%u threads spawned\n", Threadpool.threadCount);

	double t1;
	double t2;
	// Main loop
	running = 1;
	while(running)
	{
		t1 = glfwGetTime();
		glClear(GL_COLOR_BUFFER_BIT);

		glfwPollEvents();
		PollMouse(&Mouse, elapsedTimeMs);
		PollKeysImmediate(Win, elapsedTimeMs);


		// This function must be called before any work is done on the texture buffer since it may deallocate the texture
		UpdateResolutionIfNecessary();
		
		UpdateTexturemodeIfNecessary(&MainTexture);
		
		pthread_barrier_wait(&barrierMain);
		pthread_barrier_wait(&barrierWorker);
		Texture2DReload(&MainTexture);

		// Wait for threads to complete before updating global iteration variable
		UpdateIterationsIfNecessary();

		glUseProgram(MainRect.shader);
		glBindTexture(GL_TEXTURE_2D, MainTexture.id);
		glBindBuffer(MainRect.vbo, GL_ARRAY_BUFFER);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, MainRect.ebo);

		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
		glBindTexture(GL_TEXTURE_2D, 0);

		glfwSwapBuffers(Win);
		t2 = glfwGetTime();
		elapsedTimeMs = t2 - t1;
		// Uncomment this for fps statistics
		// FpsReport(elapsedTimeMs * 1000.0);
	}

	// End main
	glfwTerminate();
	Cleanup();
}

void PrescaledCoordinatesAllocAndGen(u32 virtual_width, u32 virtual_height)
{
	//   x0 := scaled x coordinate of pixel (scaled to lie in the Mandelbrot X scale (-2.5, 1))
	//   y0 := scaled y coordinate of pixel (scaled to lie in the Mandelbrot Y scale (-1, 1))

	// In short, these are normalised coordinates from screen space to clip space (if I am not misusing this terminology)

	u32 i;
	double j;
	xs = PlatformAlloc(virtual_width, sizeof *xs);
	ys = PlatformAlloc(virtual_height, sizeof *ys);
	// 3.5
	for(i = 0, j = 0; i < virtual_width; i++, j++)
	{
		xs[i] = (j / (double)(virtual_width - 1) * 3.5) - 2.5;
	}
	for(i = 0, j = 0; i < virtual_height; i++, j++)
	{
		ys[i] = (j / (double)(virtual_height - 1) * 2.0) - 1.0;
	}
}
void CbResize(GLFWwindow *Window, int x, int y)
{
	glViewport(0, 0, x, y);
	Screen.w = x;
	Screen.h = y;
	if(!IsFullscreen())
	{
		Screen.windowedWidth = x;
		Screen.windowedHeight = y;
	}
}

void CbKeys(GLFWwindow *Window, int key, int scancode, int action, int mod)
{
	// Quit
	KEYPRESS(KEY_Q)
	{
		running = 0;
	}

	// Resize
	KEYPRESS(KEY_TAB)
	{
		if(mod == GLFW_MOD_SHIFT)
		{
			resNeedsUpdate = RES_CYCLE_DOWN;
		}
		else
		{
			resNeedsUpdate = RES_CYCLE_UP;
		}
	}

	// Fullscreen
	KEYPRESS(KEY_F)
	{
		if(Options[OPT_FULLSCREEN])
		{
			// glfwSetWindowMonitor(Win, NULL, 0, 0, 640, 480, GLFW_DONT_CARE);
			glfwSetWindowMonitor(Win, NULL, Screen.windowedX, Screen.windowedY, Screen.windowedWidth, Screen.windowedHeight, GLFW_DONT_CARE);
		}
		else
		{
			glfwSetWindowMonitor(Win, Mon, 0, 0, Vidmode->width, Vidmode->height, GLFW_DONT_CARE);
		}
			Options[OPT_FULLSCREEN] ^= 1;
	}

	// Reset
	KEYPRESS(KEY_R)
	{
		scale = 1.0;
		scaleInverse = 1.0;
		offX = 0;
		offY = 0;
		cameraWasUpdated = 1;
	}

	// Decrease iterations
	KEYPRESS(KEY_Z)
	{
		iterationsNeedUpdate = ITERATION_MASK_DECREASE;
	}

	// Increases iterations
	KEYPRESS(KEY_X)
	{
		iterationsNeedUpdate = ITERATION_MASK_INCREASE;
	}

	// Set texturemode 
	KEYPRESS(KEY_K)
	{
		texturemodeNeedsUpdate = 1;
		ActiveTexturemode = GL_TEXTUREMODE_LINEAR;
	}

	KEYPRESS(KEY_L)
	{
		texturemodeNeedsUpdate = 1;
		ActiveTexturemode = GL_TEXTUREMODE_NEAREST;
	}

	// Assign various equations for the threadpool to solve for
	KEYPRESS(KEY_1)
	{
		ThreadpoolWorkfuncAssign(&Threadpool, PoolMandelIntrinFp);
	}
	KEYPRESS(KEY_2)
	{
		ThreadpoolWorkfuncAssign(&Threadpool, PoolShipIntrinFp);
	}

	KEYPRESS(KEY_3)
	{
		ThreadpoolWorkfuncAssign(&Threadpool, PoolMandelDefaultFp);
	}

	KEYPRESS(KEY_4)
	{
		ThreadpoolWorkfuncAssign(&Threadpool, PoolShipDefaultFp);
	}

}

void PollKeysImmediate(GLFWwindow *Window, double dt)
{
	float scalespeed;
	float movespeed;
	scalespeed = .75;
	movespeed = 2.0;


	KEYIMM(KEY_EQUAL)
	{
		scale = (scale + ((scalespeed * scale) * dt  ));
		scaleInverse = 1.0 / scale;
		cameraWasUpdated = 1;
	}
	KEYIMM(KEY_MINUS)
	{
		scale = (scale - ((scalespeed * scale) * dt  ));
		scaleInverse = 1.0  / scale;
		cameraWasUpdated = 1;
	}

	KEYIMM(KEY_W)
	{
		offY -= (movespeed * 1.0 / scale) * dt;
		cameraWasUpdated = 1;
	}

	KEYIMM(KEY_S)
	{
		offY += (movespeed * 1.0 / scale) * dt;
		cameraWasUpdated = 1;
	}

	// @Cleanup: Make X move with aspect ratio
	KEYIMM(KEY_A)
	{
		offX -= (movespeed * 1.6 / scale) * dt;
		cameraWasUpdated = 1;
	}
	KEYIMM(KEY_D)
	{
		offX += (movespeed * 1.6 / scale) * dt;
		cameraWasUpdated = 1;
	}
}

void PalleteAllocAndGen(u32 colorCount)
{
	u32 i;
	u32 brightness;

	pallete = PlatformAlloc(colorCount, sizeof *pallete);
	for(i = 0; i < colorCount; i++)
	{
		// Inverse (start at 100% white and go up in darkness as iterations go up
		brightness = ( 1.0 - ((double) i / (double)(colorCount - 1)) ) * (double)0xFF;

		// Regular brightness (Lower iterations = lower brightness)
		// brightness = (((double) i / (double)(colorCount - 1)) ) * (double)0xFF;
		
		// White colour
		pallete[i] = brightness | (brightness << 8) | (brightness << 16) | (0xFF << 24);

		// Yellow colour
		// pallete[i] = 0 | (brightness << 8) | (brightness << 16) | (0xFF << 24);
		
		// Red colour
		// pallete[i] = 0 | (brightness << 16) | (0xFF << 24);
		
		// Dark blue colour
		// pallete[i] = i;
	}
	// If iterations are max
	// pallete[i - i] &= 0x0;
}

rect_t RectCreate(char *sourceVert, char *sourceFrag)
{
	GLuint shader;
	GLuint vbo;
	GLuint ebo;
	GLuint vao;
	GLuint uSampler;
	rect_t Rect;

	shader = ShaderProgramCreate(sourceVert, sourceFrag);
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);
	glGenBuffers(1, &ebo);

	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof verticies, verticies, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof indicies, indicies, GL_STATIC_DRAW);

	// Verticies
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, VERT_STRIDE * sizeof *verticies, (void *)0);
	glEnableVertexAttribArray(0);

	// Colours
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, VERT_STRIDE * sizeof *verticies, (void *)(2 * sizeof(float)));
	glEnableVertexAttribArray(1);

	// Texture coordinates
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, VERT_STRIDE * sizeof *verticies, (void *)(5 * sizeof(float)));
	glEnableVertexAttribArray(2);


	glUseProgram(shader);
	uSampler = glGetUniformLocation(shader, "tex1");

	glUniform1i(uSampler, 0);

	Rect.uSampler = uSampler;
	Rect.vbo = vbo;
	Rect.ebo = ebo;
	Rect.vao = vao;
	Rect.shader = shader;

	return Rect;
}

void TextureAllocBuffers(texture_t *Texture, u32 w, u32 h, u32 bpp)
{
	Texture->texture = PlatformAlloc(w * h * bpp, sizeof *Texture->texture);
}

void TextureFreeBuffers(texture_t *Texture)
{
	PlatformDealloc(Texture->texture);
}

void Cleanup()
{
	TextureFreeBuffers(&MainTexture);
	PlatformDealloc(pallete);
	ThreadpoolDeinit(&Threadpool);
	PrescaledCoordinatesFree();
}

texture_t Texture2DCreate(u32 w, u32 h, u32 bpp)
{
	GLuint id;
	GLuint pbo;
	texture_t Texture;
	TextureAllocBuffers(&Texture,w, h, bpp);

	glGenBuffers(1, &pbo);
	glGenTextures(1, &id);
	glBindTexture(GL_TEXTURE_2D, id);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_BGRA, GL_UNSIGNED_INT_8_8_8_8_REV, Texture.texture);

	Texture.id = id;
	Texture.pbo = pbo;
	Texture.w = w;
	Texture.h = h;
	Texture.size = w * h * bpp;
	return Texture;
}

void Texture2DReload(texture_t *Texture)
{
	glBindTexture(GL_TEXTURE_2D, Texture->id);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, Texture->pbo);
	glBufferData(GL_PIXEL_UNPACK_BUFFER, Texture->size, Texture->texture, GL_STREAM_DRAW);
	Texture->pixels = glMapBuffer(GL_PIXEL_UNPACK_BUFFER, GL_WRITE_ONLY);
	glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, Texture->w, Texture->h, 0, GL_BGRA, GL_UNSIGNED_INT_8_8_8_8_REV, 0);
}

void Texture2DResize(texture_t *Texture, u32 w, u32 h, u32 bpp)
{
	// Assumes the buffer has been freed beforehand
	TextureAllocBuffers(Texture, w, h, bpp);
	Texture->w = w;
	Texture->h = h;
	Texture->size = Texture->w * Texture->h * bpp;
}

void ThreadpoolInit(threadpool_t *Pool)
{
	int rc;
	long nThreads;
	pthread_t *threads;
	worker_t *Workers;

	rc = sysconf(_SC_NPROCESSORS_ONLN);
	if(rc == -1)
	{
		// @Cleanup Don't exit here, just dont init the threads and go back to single threaded operation
		perror("sysconf");
		exit(1);
	}
	if(rc == 1)
	{
		// @Cleanup: Just go back to single threaded mode
	}
	nThreads = rc;
	threads = PlatformAlloc(nThreads, sizeof *threads);
	Workers = PlatformAlloc(nThreads, sizeof *Workers);

	// Barriers are initialized with 1 extra thread to account for the main thread
	pthread_barrier_init(&barrierMain, NULL, nThreads + 1);
	pthread_barrier_init(&barrierWorker, NULL, nThreads + 1);
	threadtimetoquit = 0;
	Pool->Workers = Workers;
	Pool->threadCount = nThreads;
	Pool->threadIds = threads;
}

void ThreadpoolDeinit(threadpool_t *Pool)
{
	// @Cleanup I have no idea how to properly terminate threads that were in an infinite loop
	u32 i;
	int rc;
	pthread_barrier_init(&barrierMain, NULL, 1);
	pthread_barrier_init(&barrierWorker, NULL, 1);
	for(i = 0; i < Pool->threadCount; i++)
	{
		rc = pthread_detach(Pool->threadIds[i]);
		if(rc == 0)
		{
			printf("ThreadpoolDeinit: Thread %ld has been detached\n", Pool->threadIds[i]);
		}
	}
	threadtimetoquit = 1;
	PlatformDealloc(Pool->Workers);
	PlatformDealloc(Pool->threadIds);
	pthread_barrier_destroy(&barrierMain);
	pthread_barrier_destroy(&barrierWorker);
}

void ThreadpoolAssignWorkerAttributes(threadpool_t *Pool, texture_t *Texture)
{
	u32 i;
	u32 yBegin;
	u32 yEnd;
	u32 xBegin;
	u32 xEnd;
	u32 interval;

	interval = Texture->h  / Pool->threadCount;
	xBegin = 0;
	xEnd = Texture->w;

	for(i = 0; i < Pool->threadCount; i++)
	{
		yBegin = interval * i;
		yEnd = yBegin + interval;

		Pool->Workers[i].yBegin = yBegin;
		Pool->Workers[i].yEnd = yEnd;
		Pool->Workers[i].xBegin = xBegin;
		Pool->Workers[i].xEnd = xEnd;
		Pool->Workers[i].fractal = (u32 *)Texture->texture;
		Pool->Workers[i].internalId = i;
	}
}

void ThreadpoolCreateThreads(threadpool_t *Pool, void (*InitialWorkfunc)(void *arg))
{
	u32 i;
	ThreadpoolWorkfuncAssign(Pool, InitialWorkfunc);
	for(i = 0; i < Pool->threadCount; i++)
	{
		pthread_create(&Pool->threadIds[i], NULL,  ThreadpoolFuncHandle, &Pool->Workers[i]);
	}
}

void *ThreadpoolFuncHandle(void *arg)
{
	while(!threadtimetoquit)
	{
		pthread_barrier_wait(&barrierMain);
		if(cameraWasUpdated)
		{
			Threadpool.WorkerFunc(arg);
		}
		pthread_barrier_wait(&barrierWorker);
		cameraWasUpdated = 0;
	}
	return NULL;
}

void ThreadpoolWorkfuncAssign(threadpool_t *Pool, void (*Workfunc)(void *arg))
{
	Assert(Workfunc != NULL);
	Pool->WorkerFunc = Workfunc;
	cameraWasUpdated = 1;
}


// Uses Intel's SSE2/3 and AVX features
void PoolMandelIntrinFp(void *arg)
{
	// @Optimize: Consider making the variables static since this function is called every frame
	worker_t *Worker;
	Worker = arg;

	u32 py;
	u32 px;
	// scale = 1.5;
	int timetoleave;
	int leavemask;

	__m256d *_xs;
	__m256d _ys;
	__m256d _xx;
	__m256d _yy;
	__m256d _xxyy;
	__m256d _x;
	__m256d _y;
	__m256d _s;
	__m256d _iterationMax;
	__m256d _n;
	__m256d _tempx;
	__m256d _tempy;
	__m256d _offX;
	__m256d _offY;

	__m256d _constant2;
	__m256d _constant4;
	__m256d _constant1;

	__m256d _constantScaleX;
	__m256d _constantScaleY;

	__m256d _mask4;
	__m256d _maskiter;
	__m256d _maskadd;

	// Setup
	_xs = (__m256d *)xs;
	_offX = _mm256_set1_pd(offX);
	_offY = _mm256_set1_pd(offY);

	_constant2 = _mm256_set1_pd(2.0);
	_constant4 = _mm256_set1_pd(4.0);
	_constant1 = _mm256_set1_pd(1.0);

	//@Cleanup -1 -> things get weird because of array overflow. Need to fix
	_iterationMax = _mm256_set1_pd(ITERATIONS);
	_s = _mm256_set1_pd(scaleInverse);

	for(py = Worker->yBegin; py < Worker->yEnd; py++)
	{
		_ys = _mm256_set1_pd(ys[py]);
		_constantScaleY = _mm256_mul_pd(_ys, _s);
		_constantScaleY = _mm256_add_pd(_offY, _constantScaleY);

		for(px = Worker->xBegin; px < Worker->xEnd / 4; px++)
		{
			_n = _mm256_setzero_pd();
			_xx = _mm256_setzero_pd();
			_yy = _mm256_setzero_pd();
			_y = _mm256_setzero_pd();
			_x = _mm256_setzero_pd();
			_constantScaleX = _mm256_mul_pd(_xs[px], _s);
			_constantScaleX = _mm256_add_pd(_constantScaleX , _offX);
			timetoleave = 0;

			while(!timetoleave)
			{
				// tempx = xx - yy + (xs[px] * s + offX);
				// y = (2 * x * y) + (ys[py] * s + offY);
				_tempx = _mm256_sub_pd(_xx, _yy);
				_tempx = _mm256_add_pd(_tempx, _constantScaleX);
				_tempy = _mm256_mul_pd(_constant2, _x);
				_tempy = _mm256_mul_pd(_y, _tempy);
				_y = _mm256_add_pd(_tempy, _constantScaleY);

				// x = tempx;
				// xx = x * x;
				// // yy = y * y;
				_x = _tempx;
				_xx = _mm256_mul_pd(_x, _x);
				_yy = _mm256_mul_pd(_y, _y);


				// while(((xx) + (yy)) <= 4 && (iteration < maxiter))
				_xxyy = _mm256_add_pd(_xx, _yy);
				_mask4 = _mm256_cmp_pd(_xxyy, _constant4, _CMP_LT_OQ);
				_maskiter = _mm256_cmp_pd(_iterationMax, _n, _CMP_GT_OQ);
				_maskiter = _mm256_and_pd(_mask4, _maskiter);
				_maskadd = _mm256_and_pd(_maskiter, _constant1);

				_n = _mm256_add_pd(_n, _maskadd); // n++
				leavemask = _mm256_movemask_pd(_maskiter);
				// leavemask = _mm256_testz_pd(_maskiter, _mask4);
				if(!leavemask)
				{
					timetoleave = 1;
				}
			}
			Worker->fractal[(py * Screen.resx) + (px * 4) + 0] = pallete[(u32)_n[0]];
			Worker->fractal[(py * Screen.resx) + (px * 4) + 1] = pallete[(u32)_n[1]];
			Worker->fractal[(py * Screen.resx) + (px * 4) + 2] = pallete[(u32)_n[2]];
			Worker->fractal[(py * Screen.resx) + (px * 4) + 3] = pallete[(u32)_n[3]];
		}
	}
}


void PoolShipIntrinFp(void *arg)
{
	// @Optimize: Consider making the variables static since this function is called every frame
	worker_t *Worker;
	Worker = arg;


	u32 py;
	u32 px;
	// scale = 1.5;
	int timetoleave;
	int leavemask;

	__m256d *_xs;
	__m256d _ys;
	__m256d _xx;
	__m256d _yy;
	__m256d _xxyy;
	__m256d _x;
	__m256d _y;
	__m256d _s;
	__m256d _iterationMax;
	__m256d _n;
	__m256d _tempx;
	__m256d _tempy;
	__m256d _offX;
	__m256d _offY;

	__m256d _constant2;
	__m256d _constant4;
	__m256d _constant1;

	__m256d _constantScaleX;
	__m256d _constantScaleY;

	__m256d _mask4;
	__m256d _maskiter;
	__m256d _maskadd;
	__m256i _masksign;

	// Setup
	_xs = (__m256d *)xs;
	_offX = _mm256_set1_pd(offX);
	_offY = _mm256_set1_pd(offY);

	_constant2 = _mm256_set1_pd(2.0);
	_constant4 = _mm256_set1_pd(4.0);
	_constant1 = _mm256_set1_pd(1.0);
	_masksign = _mm256_set1_epi64x(0x7FFFFFFFFFFFFFFF);

	_iterationMax = _mm256_set1_pd(ITERATIONS);
	_s = _mm256_set1_pd(scaleInverse);

	for(py = Worker->yBegin; py < Worker->yEnd; py++)
	{
		_ys = _mm256_set1_pd(ys[py]);
		_constantScaleY = _mm256_mul_pd(_ys, _s);
		_constantScaleY = _mm256_add_pd(_offY, _constantScaleY);

		for(px = Worker->xBegin; px < Worker->xEnd / 4; px++)
		{
			_n = _mm256_setzero_pd();
			_xx = _mm256_setzero_pd();
			_yy = _mm256_setzero_pd();
			_y = _mm256_setzero_pd();
			_x = _mm256_setzero_pd();
			_constantScaleX = _mm256_mul_pd(_xs[px], _s);
			_constantScaleX = _mm256_add_pd(_constantScaleX , _offX);
			timetoleave = 0;

			while(!timetoleave)
			{
				// tempx = xx - yy + (xs[px] * s + offX);
				// y = (2 * x * y) + (ys[py] * s + offY);
				_tempx = _mm256_sub_pd(_xx, _yy);
				_tempx = _mm256_add_pd(_tempx, _constantScaleX);
				_tempy = _mm256_mul_pd(_constant2, _x);
				_tempy = _mm256_mul_pd(_y, _tempy);
				_tempy = _mm256_and_pd(_tempy, (__m256d)_masksign);
				_y = _mm256_add_pd(_tempy, _constantScaleY);

				// x = tempx;
				// xx = x * x;
				// // yy = y * y;
				_x = _tempx;
				_xx = _mm256_mul_pd(_x, _x);
				_yy = _mm256_mul_pd(_y, _y);


				// while(((xx) + (yy)) <= 4 && (iteration < maxiter))
				_xxyy = _mm256_add_pd(_xx, _yy);
				_mask4 = _mm256_cmp_pd(_xxyy, _constant4, _CMP_LT_OQ);
				_maskiter = _mm256_cmp_pd(_iterationMax, _n, _CMP_GT_OQ);
				_maskiter = _mm256_and_pd(_mask4, _maskiter);
				_maskadd = _mm256_and_pd(_maskiter, _constant1);

				_n = _mm256_add_pd(_n, _maskadd); // n++
				leavemask = _mm256_movemask_pd(_maskiter);
				if(!leavemask)
				{
					timetoleave = 1;
				}
			}
			Worker->fractal[(py * Screen.resx) + (px * 4) + 0] = pallete[(u32)_n[0]];
			Worker->fractal[(py * Screen.resx) + (px * 4) + 1] = pallete[(u32)_n[1]];
			Worker->fractal[(py * Screen.resx) + (px * 4) + 2] = pallete[(u32)_n[2]];
			Worker->fractal[(py * Screen.resx) + (px * 4) + 3] = pallete[(u32)_n[3]];
		}
	}
}

// If SSE2/3 and AVX are not supported
// This is just regular C code that calculates the mandelbrot set
void PoolMandelDefaultFp(void *arg)
{

	worker_t *Worker;
	Worker = arg;

	u32 py;
	u32 px;
	double xx;
	double yy;
	double x;
	double y;
	double tempx;
	int iteration;
	int maxiter = ITERATIONS - 1;
	double s;
	s = scaleInverse;

	for(py = Worker->yBegin; py < Worker->yEnd; py++)
	{
		for(px = Worker->xBegin; px < Worker->xEnd; px++)
		{
			iteration = 0;
			xx = 0;
			yy = 0;
			x = 0;
			y = 0;
			while(((xx) + (yy)) <= 4 && (iteration < maxiter))
			{
				tempx = xx - yy + (xs[px] * s + offX);
				y = (2 * x * y) + (ys[py] * s + offY);

				x = tempx;
				xx = x * x;
				yy = y * y;
				iteration++;
			}
			Worker->fractal[(py * Screen.resx) + px] = pallete[iteration];
		}
	}
}

// If SSE2/3 and AVX are not supported
// This is just regular C code that calculates the 'ship' set
void PoolShipDefaultFp(void *arg)
{

	worker_t *Worker;
	Worker = arg;

	u32 py;
	u32 px;
	double xx;
	double yy;
	double x;
	double y;
	double tempx;
	int iteration;
	int maxiter = ITERATIONS - 1;
	double s;
	s = scaleInverse;


	for(py = Worker->yBegin; py < Worker->yEnd; py++)
	{
		for(px = Worker->xBegin; px < Worker->xEnd; px++)
		{
			iteration = 0;
			xx = 0;
			yy = 0;
			x = 0;
			y = 0;
			while(((xx) + (yy)) <= 4 && (iteration < maxiter))
			{
				tempx = xx - yy + (xs[px] * s + offX);
				y = (2 * x * y) + (ys[py] * s + offY);
				x = tempx;

				y = fabs(y);
				x = fabs(x);
				xx = x * x;
				yy = y * y;

				iteration++;
			}
			Worker->fractal[(py * Screen.resx) + px] = pallete[iteration];
		}
	}
}

void CbMouseMove(GLFWwindow *Window, double x, double y)
{
	Mouse.raw.x = x;
	Mouse.raw.y = y;

	if((glfwGetMouseButton(Window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS)
	||  (glfwGetMouseButton(Window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS))
	{
		MouseToWorld(&Mouse);
	}
}

void CbMousePress(GLFWwindow *Window, int button, int action, int mod)
{
	// Not filtering for buttons at the moment
	// This may become needed later but not for now
	if(action == GLFW_PRESS)
	{
		Mouse.held = 1;
	}
	else if(action == GLFW_RELEASE)
	{
		Mouse.held = 0;
	}
}

void PollMouse(mouse_t *M, double dt)
{

	// @Cleanup The mouse poll code is really hacked together
	// Condering saving the state of each required mouse button
	float scalespeed;
	float movespeed;
	scalespeed = .60;
	movespeed = 1.25;
	if(M->held)
	{
		double baselineX = Screen.resx / 2;
		double baselineY = Screen.resy / 2;

		M->percX = (double)(M->world.x -  baselineX) / baselineX;
		M->percY = -(double)(M->world.y -  baselineY) / baselineY;
		// @Cleanup Fix the aspect ratio factor. Something is off here
		// Zooms in
		if(glfwGetMouseButton(Win, GLFW_MOUSE_BUTTON_LEFT))
		{
			offY -= ((movespeed * (1.0 / scale)) * dt) * (M->percY);
			offX += ((movespeed * (1.6 / scale)) * dt) * (M->percX);
			scale = (scale + ((scalespeed * scale) * dt  ));
			scaleInverse = 1.0 / scale;
			cameraWasUpdated = 1;

		}
		// Zooms out
		else if(glfwGetMouseButton(Win, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS)
		{
			offY -= ((movespeed * 1.0 / scale) * dt) * (M->percY);
			offX -= ((movespeed * 1.6 / scale) * dt) * (M->percX);
			scale = (scale - ((scalespeed * scale) * dt  ));
			scaleInverse = 1.0 / scale;
			cameraWasUpdated = 1;
		}
	}
}

void MouseToWorld(mouse_t *M)
{
	M->world.x = (double)Screen.resx * (M->raw.x / (double)Screen.w);
	M->world.y = (double)Screen.resy * (M->raw.y / (double)Screen.h);
}

INLINE_ALWAYS void UpdateIterationsIfNecessary()
{
	if(iterationsNeedUpdate)
	{
		switch(iterationsNeedUpdate)
		{
			case ITERATION_MASK_DECREASE:
				ITERATIONS = (ITERATIONS > 64) ? ITERATIONS - ITERATIONS_STEP : ITERATIONS_DEFAULT;
				ProgPrint("Iterations decreased to %d\n", ITERATIONS);
				break;
			case ITERATION_MASK_INCREASE:
				ITERATIONS += ITERATIONS_STEP;
				ProgPrint("Iterations increased to %d\n", ITERATIONS);
				break;
		}
		cameraWasUpdated = 1;
		iterationsNeedUpdate = 0;
		// This fixes the overflow but the cool colours disappear
		// Remove these two lines for a weird overflow bug that produces interesting colours
		PlatformDealloc(pallete);
		PalleteAllocAndGen(ITERATIONS);
	}
}

void UpdateResolutionIfNecessary()
{
	if(resNeedsUpdate)
	{
		if(resNeedsUpdate & RES_CYCLE_UP)
		{
			resIndex = (resIndex + 1) % RESOLUTIONS_TOTAL;
		}
		else if (resNeedsUpdate & RES_CYCLE_DOWN)
		{
			resIndex = (resIndex  == 0) ? RESOLUTIONS_TOTAL - 1 : resIndex - 1;
		}
		PrescaledCoordinatesFree();
		TextureFreeBuffers(&MainTexture);

		Screen.resx = Resolutions[resIndex].width;
		Screen.resy = Resolutions[resIndex].height;

		PrescaledCoordinatesAllocAndGen(Screen.resx, Screen.resy);
		Texture2DResize(&MainTexture, Screen.resx, Screen.resy, BPP);
		ThreadpoolAssignWorkerAttributes(&Threadpool, &MainTexture);
		MouseToWorld(&Mouse);
		resNeedsUpdate = 0;
		cameraWasUpdated = 1;
		ProgPrint("Changed texture resolution to %ux%u\n", Screen.resx, Screen.resy);
	}
}


void PrescaledCoordinatesFree()
{
	PlatformDealloc(xs);
	PlatformDealloc(ys);
}

#define HRT(m) case(m): return #m;
const char *HumanReadableTexturemode(gl_texturemode_t Mode)
{
	switch(Mode)
	{
		HRT(GL_TEXTUREMODE_NEAREST);
		HRT(GL_TEXTUREMODE_LINEAR);
		default:
		Unreachable("Invalid handling of texture mode in HumanReadableTexturemode");
	}
}

INLINE_ALWAYS void UpdateTexturemodeIfNecessary(texture_t *Texture)
{
	if(texturemodeNeedsUpdate)
	{
		glBindTexture(GL_TEXTURE_2D, Texture->id);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, TexturemodeLookup[ActiveTexturemode]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, TexturemodeLookup[ActiveTexturemode]);
		glBindTexture(GL_TEXTURE_2D, 0);
		texturemodeNeedsUpdate = 0;
		ProgPrint("Switched texture mode to %s\n", HumanReadableTexturemode(ActiveTexturemode));
	}
}

void ProgPrint(const char *fmt, ...)
{
	va_list vl;
	printf("%s: ", PROG_NAME);
	va_start(vl, fmt);
	vprintf(fmt, vl);
	va_end(vl);
}

void CbWindowPosition(GLFWwindow *Window, int xpos, int ypos)
{
	if(!IsFullscreen())
	{
		Screen.windowedX = xpos;
		Screen.windowedY = ypos;
	}
}

void CbWindowClose(GLFWwindow *Window)
{
	running = 0;
}

