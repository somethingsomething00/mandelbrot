#ifndef _TYPES_H_
#define _TYPES_H_

typedef uint32_t u32;
typedef uint8_t u8;

typedef double f64;
typedef float f32;

#undef true
#undef false
#define true 1
#define false 0

#endif /* _TYPES_H_ */
