#ifndef _LOADER_H_
#define _LOADER_H_


#define LOADER_FATAL(...) do\
{\
	fprintf(stderr, "FATAL: %s %d: ", __FILE__, __LINE__);\
	fprintf(stderr, __VA_ARGS__);\
	exit(1);\
}\
while(0)

#define LOADER_WARN(...) do\
{\
	fprintf(stderr, "WARN: %s %d: ", __FILE__, __LINE__);\
	fprintf(stderr, __VA_ARGS__);\
}\
while(0)

char *ShaderOpen(char *path);
void ShaderCheck(GLuint shader, char *file);
void ProgramCheck(GLuint program, char *sourceVert, char *sourceFrag);
GLuint ShaderProgramCreate(char *sourceVert, char *sourceFrag);

#endif /* _LOADER_H_ */
