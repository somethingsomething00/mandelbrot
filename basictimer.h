#ifndef _BASICTIMER_H_
#define _BASICTIMER_H_

typedef struct
{
	char *label;
	double begin;
	double end;
	double elapsed;
	u32 frameCount; /* Will overflow eventually */
	struct timespec TsBegin;
	struct timespec TsEnd;
} PerfTimer;

void PerfReport();
__attribute__((noinline)) void PerfBegin(char *label);
 __attribute__((noinline))   void PerfEnd();
#endif /* _BASICTIMER_H_ */
