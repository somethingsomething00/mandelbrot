#ifndef _FPSCOUNTER_H_
#define _FPSCOUNTER_H_


// Expects delta time in milliseconds

static void FpsReport(float deltams)
{
	static float _fpsReportaccumulator = 0;
	static unsigned int _fpsReportfpsCounter = 0;
	_fpsReportaccumulator += deltams;
	_fpsReportfpsCounter++;
	if(_fpsReportfpsCounter == 240)
	{
		_fpsReportaccumulator /= 240.0f;
		printf("%.4f ms per frame [%.2f fps]\n", _fpsReportaccumulator, 1000.0f / _fpsReportaccumulator);
		_fpsReportaccumulator = 0;
		_fpsReportfpsCounter = 0;
	}
}
#endif /* _FPSCOUNTER_H_ */
