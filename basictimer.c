#ifndef _BASICTIMER_C_
#define _BASICTIMER_C_

static PerfTimer Ptimer = {0};

__attribute__((noinline)) void PerfBegin(char *label)
{
	Ptimer.label = label;
	clock_gettime(CLOCK_MONOTONIC, &Ptimer.TsBegin);
}

 __attribute__((noinline))   void PerfEnd()
{
	clock_gettime(CLOCK_MONOTONIC, &Ptimer.TsEnd);
	Ptimer.frameCount++;
	double nsecElapsed;
	double secElapsed;

	secElapsed = Ptimer.TsEnd.tv_sec - Ptimer.TsBegin.tv_sec;
	nsecElapsed = (double)(Ptimer.TsEnd.tv_nsec - Ptimer.TsBegin.tv_nsec) / 1E9;
	Ptimer.elapsed += secElapsed + nsecElapsed;
}

void PerfReport()
{
	if(!Ptimer.label) exit(0);
	printf("[%s]\n", Ptimer.label);
	printf("Frames captured: %u\n", Ptimer.frameCount);
	printf("%f sec per frame on average\n", Ptimer.elapsed / (double)Ptimer.frameCount);
	printf("%f msec per frame on average\n", (Ptimer.elapsed / (double)Ptimer.frameCount) * 1000.0);
}
#endif /* _BASICTIMER_C_ */
