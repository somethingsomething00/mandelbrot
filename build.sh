#!/bin/sh
cc="gcc"
src="mandelbrot.c"
bin="mandelbrot"
libs="-lGL -lGLEW -lglfw -lm -lpthread"
cflags="-mtune=native -march=native"
cflags+=" -mavx -msse2"
optimize_flags="-O2"

compile_command="$cc $src $libs $cflags $optimize_flags -o $bin"
echo $compile_command
$compile_command

