# Mandelbrot
A mandelbrot set renderer written in C, using OpenGL 3.3.

![thumbnail](./mandel00.png)

## Dependencies
- GCC or Clang (GCC is used by default)
- glfw3
- glew
- OpenGL 3.3

## To build
```console
./build.sh
```

## To run
```console
./mandelbrot
```

## Controls
`w/a/s/d` Move around the set.

`=` Zoom in.

`-` Zoom out.

`q` Quit.

`f` Toggle fullscreen.

`r` Reset zoom level and x/y offset to default values.

`LMB` Zoom in (zooms towards the mouse cursor).

`RMB` Zoom out (zooms away from the mouse cursor).

`z` Decrease iteration count by 32.

`x` Increase iteration count by 32.

`Tab` Cycle texture resolution up.

`Shift-Tab` Cycle texture resolution down.

`k` Enable texture filtering.

`l` Disable texture filtering.

`1` Calculate the mandelbrot set using intrinsics.

`2` Calculate the 'burning ship' set using intrinsics.

`3` Calculate the mandelbrot set using native C code.

`4` Calculate the 'burning ship' set using native C code.


## Goals of this project
- Learn how to use textures in OpenGL 3.3.
- Learn how to create and load basic shaders.
- Learn multithreading and memory barriers.
- Learn how to code with processor intrinsics (SSE2 / AVX).

## Known bugs
- The aspect ratio for the mouse zoom is a bit off.

## Future ideas
Add more equations to the renderer!

## References
[Psudocode of the Mandelbrot Set](https://en.wikipedia.org/wiki/Mandelbrot_set#Computer_drawings)

[Brute Force Processing](https://www.youtube.com/watch?v=PBvLs88hvJ8)

[Intrinsic Functions - Vector Processing Extensions](https://www.youtube.com/watch?v=x9Scb5Mku1g)

[Intel Intrinsics Guide](https://www.intel.com/content/www/us/en/docs/intrinsics-guide/index.html)

[Manual Page Project for Intrinsics](https://github.com/WojciechMula/man-intrinsics)

[Using barriers with the pthreads library](https://youtube.com/watch?v=zhErv2CPqiY)

[Xaos Fractal Renderer](https://xaos-project.github.io/)

[GLFW3 documentation](https://www.glfw.org/docs/latest/)

[OpenGL documentation](https://docs.gl/)
